import Head from 'next/head'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Paper from '@mui/material/Paper'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState } from 'react'

export default function Home() {
  const router = useRouter()
  const [idCode, setIdCode] = useState('');

  return (
    <Box className="container">

      <Head>
        <title>Ronijakaardi kontroll</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
      </Head>



      <Paper elevation={5} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', padding: '30px', maxWidth:'400px' }}>
        <Typography align={'center'} variant={'h6'} >Eesti Ronimisliit</Typography>
        <Box sx={{padding:'20px'}} />
        <Typography align={'center'} variant={'h4'} >Ronijakaardi otsing</Typography>
        <Box sx={{ padding:'20px'}}/>
        <Typography>Siin leheküljel saab kontrollida, kas Eesti Ronimisliit on isikule omistanud ronijakaardi.</Typography>
        <Box sx={{ paddingTop:'20px'}}/>
        <TextField
          required
          id="outlined-required"
          label="Isikukood"
          onChange={(e) => setIdCode(e.target.value)}
        />
        <Box sx={{ paddingTop:'10px'}}/>
        <Button variant="contained" onClick={() => router.push('/search/'+idCode)}>Otsi</Button>
      </Paper>
        {/* <Header title="Welcome to my app!" />
        <p className="description">
          Get started by editing <code>pages/index.js</code>
        </p> */}
 

      {/* <Footer /> */}
    </Box>
  )
}
