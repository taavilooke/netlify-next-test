import Head from 'next/head'
import Link from 'next/link'
import Paper from '@mui/material/Paper'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'

import { useRouter } from 'next/router'

export default function ClimberCardMissing() {
    const router = useRouter()
    const { idCode } = router.query
    return (
        <Box className="container">

        <Head>
          <title>Ronijakaardi kontroll</title>
          <link rel="icon" href="/favicon.ico" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
        </Head>
  
  
        <Paper elevation={5} sx={{ display: 'flex', flexDirection: 'column', justifyContents: 'center', padding: '30px', maxWidth:'400px', backgroundColor: '#fff'}}>
            <Typography align={'center'} variant={'h6'} >Ronijakaart puudub</Typography>
            <Box sx={{padding:'20px'}} />
            <Typography align={'center'}>Isikukood</Typography>
            <Typography align={'center'} variant={'h6'}>{idCode}</Typography>
            <Box sx={{padding:'5px'}} />
            <Typography>Sellele isikule ei ole Eesti Ronimisliit väljastatud ronijakaarti.</Typography>
        </Paper>
        <Box fullwidth sx={{padding:'10px'}} />
        <Link href="/">
        <Button fullWidth variant="outlined" color="secondary" sx={{maxWidth:'400px'}}>
            Tagasi
        </Button>
        </Link>
      </Box>
    )
}