import Head from 'next/head'
import Header from '@components/Header'
import Footer from '@components/Footer'
import Link from 'next/link'
import Paper from '@mui/material/Paper'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'

import { Client } from '@notionhq/client';

const GREEN_COLOR = '#58b15e';
const RED_COLOR = '#b15858';



export default function ClimberCard({idCode, name, cardType, examiner, examTime}) {
    return (
        <Box className="container">

        <Head>
          <title>Ronijakaardi kontroll</title>
          <link rel="icon" href="/favicon.ico" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"/>
        </Head>
  
  
        <Paper elevation={5} sx={{ display: 'flex', flexDirection: 'column', justifyContents: 'center', padding: '30px', maxWidth:'400px', backgroundColor: cardType ? RED_COLOR : GREEN_COLOR}}>
            <Typography align={'center'} variant={'h6'} >{cardType ? 'Punane ronijakaart' : 'Roheline ronijakaart'}</Typography>
            <Box sx={{padding:'20px'}} />
            <Typography align={'center'} variant={'h4'} >{name}</Typography>
            <Box sx={{padding:'5px'}} />
            <Typography align={'center'}>Isikukood</Typography>
            <Typography align={'center'} variant={'h6'}>{idCode}</Typography>
            <Box sx={{padding:'5px'}} />
            <Typography>on läbinud ülaltjulgestusega ronimise eksami.</Typography>
            <Box sx={{padding:'20px'}}/>

            <Typography align={'center'} variant='caption' >Eksamineerija</Typography>
            <Typography align={'center'} >{examiner}</Typography>
            <Box sx={{padding:'5px'}}/>
            <Typography align={'center'} variant='caption'>Eksami aeg</Typography>
            <Typography align={'center'}>{examTime}</Typography>
            
            <Box sx={{padding:'20px'}}/>
            <Typography>{'Ronimisliit kinnitab, et ronijakaardi omanikul on piisavad osused iseseisvaks ronimiseks ja julgestamiseks kaljuronimisseinal '+ (cardType ? 'altjulgestuses' : 'ülaltjulgestuses') + '.'}</Typography>
        </Paper>
        <Box fullwidth sx={{padding:'10px'}} />
        <Link href="/">
        <Button fullWidth variant="outlined" color="secondary" sx={{maxWidth:'400px'}}>
            Tagasi
        </Button>
        </Link>
      </Box>
    )
}

const DB_PROPERTY_IDCODE = "Isikukood";
const DB_PROPERTY_NAME = "Nimi";
const DB_PROPERTY_CARDTYPE = "Kaardi tüüp";
const DB_PROPERTY_EXAMTIME = "Eksami aeg"
const DB_PROPERTY_EXAMINER = "Eksamineerija";

const GREEN_CODE = 'Roheline';
const RED_CODE = 'Punane';

export async function getServerSideProps(context) {
    const idCode = context.params.idCode;

    const notion = new Client({
        auth: process.env.NOTION_TOKEN,
    })

    const myPage = await notion.databases.query({
        database_id: process.env.NOTION_DATABASE_ID,
        filter: {
            property: DB_PROPERTY_IDCODE,
            text: {
            equals: idCode,
            },
        },
    });

    if (!myPage.results.length){
      return {
        redirect: {
          destination: '/nocard/'+idCode,
          permanent: false,
        },
      }
    }

    const [dbEntry] = myPage.results;
    const name = dbEntry.properties[DB_PROPERTY_NAME].rich_text[0].plain_text;
    const examiner = dbEntry.properties[DB_PROPERTY_EXAMINER].rich_text[0].plain_text;
    const examTime = dbEntry.properties[DB_PROPERTY_EXAMTIME].date.start;

    
    const cardColor = dbEntry.properties[DB_PROPERTY_CARDTYPE].select.name;
    let cardType;
    if (cardColor == RED_CODE){
      cardType = true;
    }else if (cardColor == GREEN_CODE){
      cardType = false;
    }else{
      return {
        redirect: {
          destination: '/nocard/'+idCode,
          permanent: false,
        },
      }
    }

    return {
        props:{idCode, name, cardType, examiner, examTime}
    };
  }